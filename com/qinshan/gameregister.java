package Gameplay.com.qinshan;

import javax.swing.*;
/*
/**
 *  @author作者: 青衫烟雨客
 * @description产品描述: 游戏注册界面
 * @param: null
 * @return返回:
 * @date时间: 2023/11/2 下午 10:40
 */

import javax.swing.*;

public class gameregister extends JFrame {
    //跟注册相关的代码，都写在这个界面中
    public gameregister(){
        this.setSize(488,500);
        //设置界面的标题
        this.setTitle("拼图 注册");
        //设置界面置顶
        this.setAlwaysOnTop(true);
        //设置界面居中
        this.setLocationRelativeTo(null);
        //设置关闭模式
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //让显示显示出来，建议写在最后
        this.setVisible(true);


        getContentPane();
    }
}
