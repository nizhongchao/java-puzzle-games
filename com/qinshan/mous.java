package Gameplay.com.qinshan;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class mous extends JFrame implements MouseListener {
    public mous() {
//        JFrame
//                frame = new JFrame("鼠标事件");
        setBounds(0, 0, 500, 500);

        setAlwaysOnTop(true);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
         JButton jButton = new JButton();
        jButton.setText("鼠标点击");
        jButton.setBounds(0 ,0 ,100 , 100);
        jButton.addMouseListener(this);
        getContentPane().add(jButton);
        setLayout(null);
        setVisible(true);


    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("划入");
    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("点击");
    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("点击不松手");
    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("点击松手");
    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("划出");
    }
}
