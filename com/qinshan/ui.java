package Gameplay.com.qinshan;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class ui extends JFrame {
    JButton jt1 = new JButton();

    public ui() {
//        初始化界面
        setTitle("测试按钮");
         setBounds(0, 0, 500, 500);
        setAlwaysOnTop(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        jt1.setBounds(0, 0, 100, 50);
        jt1.setText("测试");
        jt1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (e.getSource() == jt1) {

                    jt1.setLocation(new Random().nextInt(600), new Random().nextInt(800));
                }
            }
        });
        getContentPane().add(jt1);
        setVisible(true);
        setLayout(null);
    }
}
