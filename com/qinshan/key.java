package Gameplay.com.qinshan;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class key extends JFrame implements KeyListener {
    public key() {
        setTitle("键盘输入");
        setBounds(0, 0, 500, 600);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
       setLocationRelativeTo(null);
        addKeyListener(this);
        setLayout(null);
        setVisible(true);
    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("正在按键盘");

    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void keyReleased(KeyEvent e) {
        System.out.println("松开按键");
        int keyCode = e.getKeyCode();
        System.out.println(keyCode);
        if (e.getKeyCode() == 32){
            System.out.println("空格");
        }
    }
}
