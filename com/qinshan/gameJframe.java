package Gameplay.com.qinshan;


import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import static java.lang.System.exit;
import static java.lang.System.out;

//  *  @author作者: 青衫烟雨客
//  * @description产品描述: 游戏主界面
//  * @param: null
//  * @return返回:
//  * @date时间: 2023/11/2 下午 10:39

public class gameJframe extends JFrame implements KeyListener, ActionListener {
    //创建一个二维数组
    int[][] date = new int[4][4];
    int[][] win = new int[][]{
            {1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12},
            {13, 14, 15, 0}
    };
    int x = 0;
    int y = 0;
    int count = 0;
    Random r = new Random();

    int i = r.nextInt(1, 8);
    String path = "image\\animal\\animal" + i + "\\";
    String path2 = "image\\animal\\animal";
    int g = r.nextInt(1, 10);
    int adimal = r.nextInt(1, 8);
    int sportnum = r.nextInt(1, 10);

    //创建选项条目的两个对象
    //创建一个菜单项，菜单名为“美女”
    JMenuItem girl = new JMenuItem("美女");
    //创建一个菜单项，菜单名为“动物”
    JMenuItem animal = new JMenuItem("动物");
    //创建一个菜单项，菜单名为“运动”
    JMenuItem sport = new JMenuItem("运动");
    JMenuItem jMenuItemRedo = new JMenuItem("重新游戏");
    JMenuItem jMenuItemRelogin = new JMenuItem("重新登陆");
    JMenuItem jMenuItemExit = new JMenuItem("关闭游戏");

    JMenuItem jMenuItemOfficialaccount = new JMenuItem("公众号");

    public gameJframe() {

        //初始化界面
        initjfram();
        //初始化菜单
        initjMenuBar();
        //打乱顺序
        initDate();
        //初始化界面
        initImge();

        //显示画面
        setVisible(true);
    }

    private void initDate() {
        int[] tempArry = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        //创建一个随机数
        Random random = new Random();
        //循环数组
        for (int i = 0; i < tempArry.length; i++) {
            //获取随机数
            int idnex = random.nextInt(tempArry.length);
            //交换位置
            int temp = tempArry[i];
            tempArry[i] = tempArry[idnex];
            tempArry[idnex] = temp;

        }

        //定义一个变量
//        int index = 0;
//          循环二维数组
//        for (int i = 0; i < date.length; i++) {
//            for (int j = 0; j < date[i].length; j++) {
//                //将数组中的值赋值给二维数组
//                date[i][j] = tempArry[index];
//                //变量自增
//                index++;
//            }
//        }
        //将数组使用一维数组遍历，使用
        for (int i = 0; i < tempArry.length; i++) {
            if (tempArry[i] == 0) {
                x = i / 4;
                y = i % 4;
            }
            date[i / 4][i % 4] = tempArry[i];

        }

    }

    private void initImge() {

        //  清空原本出现的图片
        getContentPane().removeAll();
        //一键胜利
        if (victory()) {
            JLabel jLabel = new JLabel(new ImageIcon("image/win.png"));
            jLabel.setBounds(200, 300, 193, 73);
            getContentPane().add(jLabel);
        }
        JLabel jLabel1 = new JLabel("步数" + count);
        jLabel1.setBounds(50, 30, 100, 20);
        getContentPane().add(jLabel1);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                int num = date[j][i];
                //创建Imag对象
                ImageIcon imageIcon = new ImageIcon(path + num + ".jpg");
                // 创建容器对象
                JLabel jLabel = new JLabel(imageIcon);
                // 设置图片定位大小
                jLabel.setBounds(105 * i + 83, 105 * j + 150, 105, 105);
                //添加斜边边框
                // 1 让图片凹下去
                // 0 让图片凸上来
                jLabel.setBorder(new BevelBorder(BevelBorder.RAISED));
                // 将图片放放到界面
                getContentPane().add(jLabel);

            }

        }
        //先加载的图片在上方，后添加的在下面
        JLabel Bground = new JLabel(new ImageIcon("image\\background.png"));
        Bground.setBounds(40, 50, 508, 560);
        getContentPane().add(Bground);

        //刷新图片
        getContentPane().repaint();
    }

    private void initjMenuBar() {


        //创建一个菜单，菜单名为“更换图片”
        JMenu changeImage = new JMenu("更换图片");
        //创建一个菜单，菜单名为“功能”
        JMenu jMenufunction = new JMenu("功能");
        //创建一个菜单，菜单名为“我们”
        JMenu jMenuAbout = new JMenu("关于我们");
        //创建一个菜单栏
        JMenuBar jMenuBar = new JMenuBar();


        //给菜单项添加鼠标监听器
        girl.addActionListener(this);
        animal.addActionListener(this);
        sport.addActionListener(this);

        changeImage.add(girl);
        //将jMenuanmil添加到jMenuItemChange中
        changeImage.add(animal);
        //将jMenuyundong添加到jMenuItemChange中
        changeImage.add(sport);


        //将选项条目项添加到菜单中
        jMenufunction.add(changeImage);
        //将重做菜单项添加到功能菜单中
        jMenufunction.add(jMenuItemRedo);
        //将重新登录菜单项添加到功能菜单中
        jMenufunction.add(jMenuItemRelogin);
        //将退出菜单项添加到功能菜单中
        jMenufunction.add(jMenuItemExit);
        //将微信公众号菜单项添加到关于菜单中
        jMenuAbout.add(jMenuItemOfficialaccount);


        //将菜单两个选项添加到菜单中
        jMenuBar.add(jMenufunction);
        jMenuBar.add(jMenuAbout);

        //给整个界面设置菜单
        setJMenuBar(jMenuBar);

        //给菜单添加动作监听器
        jMenuItemRedo.addActionListener(this);
        jMenuItemRelogin.addActionListener(this);
        jMenuItemExit.addActionListener(this);
        jMenuItemOfficialaccount.addActionListener(this);

    }


    private void initjfram() {
        //界面的标题
        setTitle("主页面");
        setSize(600, 700);
        //设置主界面置顶
        setAlwaysOnTop(true);
        //设置界面居中
        setLocationRelativeTo(null);
        //设置关闭方式
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //将属性给设置为null
        setLayout(null);
        //添加键盘监听事件
        addKeyListener(this);


    }


    //键盘监听事件

    /**
     * @param e the event to be processed
     */
    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void keyPressed(KeyEvent e) {
        //按下不松开的时候显示完整图片
        int keyCode = e.getKeyCode();
        if (keyCode == 65) {
            //将照片全部删除
            this.getContentPane().removeAll();
            //加载第一张完整图片
            JLabel all = new JLabel(new ImageIcon(path2 + i + "/" + "all.jpg"));
            all.setBounds(83, 150, 420, 420);
            this.getContentPane().add(all);

            //加载背景图片
            JLabel Bground = new JLabel(new ImageIcon("image\\background.png"));
            Bground.setBounds(40, 50, 508, 560);
            this.getContentPane().add(Bground);

            //刷新图片
            this.getContentPane().repaint();

        }
    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (victory()) {
            return;
        }
        int keyCode = e.getKeyCode();
        if (keyCode == 37) {
            if (y == 0) {
                return;
            }
            out.println("向左移动");
            count++;
            date[x][y] = date[x][y - 1];
            date[x][y - 1] = 0;
            y--;
            initImge();


        }
        if (keyCode == 38) {
            if (x == 0) {
                return;
            }
            out.println("向上移动");
            count++;
//            x y 是空白方块的坐标
//            x + 1 , y  是空白方块下面的坐标
            date[x][y] = date[x - 1][y];
            date[x - 1][y] = 0;
            x--;
            initImge();

        }
        if (keyCode == 39) {
            if (y == 3) {
                return;
            }
            out.println("向右边移动");
            count++;
            date[x][y] = date[x][y + 1];
            date[x][y + 1] = 0;
            y++;
            initImge();

        }
        if (keyCode == 40) {
            if (x == 3) {
                return;
            }
            date[x][y] = date[x + 1][y];
            date[x + 1][y] = 0;
            x++;
            out.println("向下移动");
            count++;
            initImge();

        }
        if (keyCode == 27) {
            exit(0);
        } else if (keyCode == 65) {
            initImge();
        } else if (keyCode == 87) {
            date = new int[][]{
                    {1, 2, 3, 4},
                    {5, 6, 7, 8},
                    {9, 10, 11, 12},
                    {13, 14, 15, 0}
            };
            initImge();

        }
    }

    public boolean victory() {
        for (int i = 0; i < date.length; i++) {
            for (int j = 0; j < date[i].length; j++) {
                if (win[i][j] != date[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param e the event to be processed
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == jMenuItemRedo) {
            out.println("重新游戏");
            count = 0;
            initDate();
            initImge();

            getContentPane().repaint();
        }
        if (source == jMenuItemRelogin) {
            out.println("重新登录");
            this.setVisible(false);

        }
        if (source == jMenuItemExit) {
            System.exit(0);
        }
        if (source == jMenuItemOfficialaccount) {
            out.println("公众号");
            //创建一个JDialog对象
            JDialog jDialog = new JDialog();
            //创建一个JLabel对象，并设置图片
            JLabel jLabel = new JLabel(new ImageIcon("image/about.png"));
            //设置JLabel的位置
            jLabel.setBounds(0, 0, 343, 344);
            //将JLabel添加到JDialog中
            jDialog.add(jLabel);
            //将JLabel添加到JDialog的内容面板中
            jDialog.getContentPane().add(jLabel);
            //设置JDialog的大小
            jDialog.setSize(350, 350);
            //设置JDialog总是处于最上层
            jDialog.setAlwaysOnTop(true);
            //设置JDialog的位置，使其位于屏幕中间
            jDialog.setLocationRelativeTo(null);
            //设置JDialog为模态
            jDialog.setModal(true);
            //设置JDialog可见
            jDialog.setVisible(true);
        }


        if (source == girl) {
            out.println("美女");
            path = "image/girl/girl" + g + "/";
            path2 = "image\\girl\\girl";
            i = g;
            count = 0;
            initImge();
            initDate();
            //解决再次点击样式不切换问题
            g = r.nextInt(1, 10);

        }
        if (source == animal) {
            out.println("动物");
            path = "image/animal/animal" + adimal + "/";
            path2 = "image\\animal\\animal";
            i = adimal;
            count = 0;
            initImge();
            initDate();
            //解决再次点击样式不切换问题
            adimal = r.nextInt(1, 8);
        }
        if (source == sport) {
            out.println("运动");
            path = "image/sport/sport" + sportnum + "/";
            path2 = "image\\sport\\sport";
            i = sportnum;
            count = 0;
            initImge();
            initDate();
            //解决再次点击样式不切换问题
            sportnum = r.nextInt(1, 10);
        }

    }

}